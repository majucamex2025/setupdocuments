package com.example;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//comment the above line and uncomment below line to use Chrome
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.*;

//import static org.assertj.core.api.Assertions.*;
//comment addedd

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.MutableCapabilities;
//import static org.junit.jupiter.api.Assertions.assertTrue;


import org.openqa.selenium.remote.DesiredCapabilities;


public class OpenBrowserCheckTitle {
	
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private static ThreadLocal<String> sessionId = new ThreadLocal<>();
    //protected ThreadLocal<SauceDemoNavigation> navigation = new ThreadLocal<>();

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		   String sauceUserName = "majuarcas2020";
	        String sauceAccessKey = "a84f3946-ddb6-445d-9f2d-26da08024e3b";
	        WebDriver driver;
		
		/**
         * In this section, we will configure our test to run on some specific
         * browser/os combination in Sauce Labs
         */
        DesiredCapabilities capabilities = new DesiredCapabilities();

        //set your user name and access key to run tests in Sauce
        capabilities.setCapability("username", sauceUserName);

        //set your sauce labs access key
        capabilities.setCapability("accessKey", sauceAccessKey);

        //set browser to Safari
        capabilities.setCapability("browserName", "Safari");

        //set operating system to macOS version 10.13
        capabilities.setCapability("platform", "macOS 10.13");

        //set the browser version to 11.1
        capabilities.setCapability("version", "11.1");

        //set the build name of the application
        capabilities.setCapability("build", "Onboarding Sample App - Java-Junit5");

        //set your test case name so that it shows up in Sauce Labs
        capabilities.setCapability("name", "1-first-test");

        /** If you're accessing the EU data center, use the following endpoint:.
         * https://ondemand.eu-central-1.saucelabs.com/wd/hub
         * */
        driver = new RemoteWebDriver(new URL("https://ondemand.saucelabs.com/wd/hub"), capabilities);

        //navigate to the url of the Sauce Labs Sample app
        driver.navigate().to("https://www.journaldemontreal.com/");

        //Create an instance of a Selenium explicit wait so that we can dynamically wait for an element
        WebDriverWait wait = new WebDriverWait(driver, 5);

        //wait for the user name field to be visible and store that element into a variable
        
        
        //driver.findElement(By.id("email")).sendKeys("al348058@hotmail.com");
        //driver.findElement(By.id("pass")).sendKeys("NpFBBdB202002!");
        
        //driver.findElement(By.xpath("//input[@type='submit']"));
        

        //Synchronize on the next page and make sure it loads
        //By inventoryPageLocator = By.id("inventory_container");
        //wait.until(ExpectedConditions.visibilityOfElementLocated(inventoryPageLocator));

        //Assert that the inventory page displayed appropriately
        //Boolean result = driver.findElements(inventoryPageLocator).size() > 0;
        //assertTrue(result);

	Boolean	result=true;

        /**
         * Here we teardown the driver session and send the results to Sauce Labs
         */
        if (result){
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=passed");
        }
        else {
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=failed");
        }
        driver.quit();

    }
       
}




